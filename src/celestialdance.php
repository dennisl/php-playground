<?php

/**
 * Convert key value pairs that were in the format
 * [
 *      'type' => 'mykey',
 *      'value' => 'that value
 * ]
 * into a more typical 'key' => 'value' pair
 * Expects an array of arrays [ [],[],[] ]
 */
function flatten($array) {
    $normalDict = [];
    foreach ($array as $pair) {
        $normalDict[$pair['type']] = $pair['value'];
    }
    return $normalDict;
}

/**
 * If you have multiple arrays in that weird format
 * Use this to apply flatten to all the arrays in a list
 * Expects an array of an array of arrays, [ [ [],[],[] ], [ [],[],[] ], [ [],[] ] ]
 */
function flattenArrays($array) {
    $flattenedArray = [];
    foreach ($array as $innerArray) {
        $flattenedArray[] = flatten($innerArray);
    }
    return $flattenedArray;
}

/**
 * Analyse keys in a list of dictionary and ensure all dicts have the same keys
 * Expects an array of dicts
 * 
 * For each dictionary, get the keys that are missing
 * Then set each key to empty for that dictionary
 */
function alignKeys($array) {
    $keys = compileKeys($array);
    $filledArray = $array;
    foreach ($array as $index => $dict) {
        $missingKeys = array_diff($keys, array_keys($dict));
        foreach ($missingKeys as $key) {
            $filledArray[$index][$key] = '';
        }
    }
    return $filledArray;
}

/**
 * Extracts all dictionary keys from an array of dictionaries
 * Create a list of every single key found, return uniques (remove duplicates)
 */
function compileKeys($array) {
    $keys = [];
    foreach ($array as $dicts) {
        $keys = array_merge($keys, array_keys($dicts));
    }
    return array_unique($keys);
}

/**
 * Group arrays by some value and optionally by certain keys
 * Expects an array of dicts, an array of strings and optionally a string
 * 
 * For each group specified
 * Check our array of dicts for a value matching the group
 * If matched, add the dict to the returning array
 * 
 * The use of the array_flip is a performance optimisation for large datasets
 * How would it be done if performance was a not a concern? E.g. with our small sample dataset
 */
function extractGroup($arrayToSearch, $groups, $keyFilter = null) {
    $filteredArray = [];
    foreach ($groups as $grp) {
        foreach ($arrayToSearch as $dict) {
            if (is_null($keyFilter)) {
                $haystack = array_flip($dict);
                if (isset($haystack[$grp])) {
                    $filteredArray[$grp][] = $dict;
                }
            }
            else {
                if (isset($dict[$keyFilter]) && $dict[$keyFilter] === $grp) {
                    $filteredArray[$keyFilter][] = $dict;
                }
            }
        }
        if (!isset($filteredArray[$grp]) && is_null($keyFilter)) {
            $filteredArray[$grp] = [];
        }
    }
    return $filteredArray;
}


/**
 * The "mainline" code is below, this is context specific from here onwards
 * All the functions above are fairly generic, concerning itself with data STRUCTURE not content
 */

$solarSystemEntities = [
    [
        [
            'type' => 'composition',
            'value' => 'terrestial'
        ],
        [
            'type' => 'name',
            'value' => 'Earth'
        ],
        [
            'type' => 'diameter',
            'value' => 12742
        ],
        [
            'type' => 'distance from sun',
            'value' => 150
        ],
        [
            'type' => 'moons',
            'value' => 1
        ],
        [
            'type' => 'habitable',
            'value' => 1
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'terrestial'
        ],
        [
            'type' => 'name',
            'value' => 'Mars'
        ],
        [
            'type' => 'diameter',
            'value' => 6779
        ],
        [
            'type' => 'distance from sun',
            'value' => 228
        ],
        [
            'type' => 'moons',
            'value' => 2
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'gas giant'
        ],
        [
            'type' => 'name',
            'value' => 'Jupiter'
        ],
        [
            'type' => 'diameter',
            'value' => 139820
        ],
        [
            'type' => 'distance from sun',
            'value' => 779
        ],
        [
            'type' => 'moons',
            'value' => 79
        ],
        [
            'type' => 'region',
            'value' => 'Jovian'
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'gas giant'
        ],
        [
            'type' => 'name',
            'value' => 'Saturn'
        ],
        [
            'type' => 'diameter',
            'value' => 116460
        ],
        [
            'type' => 'distance from sun',
            'value' => 1434
        ],
        [
            'type' => 'moons',
            'value' => 62
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'gas giant'
        ],
        [
            'type' => 'name',
            'value' => 'Neptune'
        ],
        [
            'type' => 'diameter',
            'value' => 49244
        ],
        [
            'type' => 'distance from sun',
            'value' => 4495
        ],
        [
            'type' => 'moons',
            'value' => 13
        ],
        [
            'type' => 'region',
            'value' => 'Jovian'
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'gas giant'
        ],
        [
            'type' => 'name',
            'value' => 'Uranus'
        ],
        [
            'type' => 'diameter',
            'value' => 50724
        ],
        [
            'type' => 'distance from sun',
            'value' => 2871
        ],
        [
            'type' => 'moons',
            'value' => 27
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'terrestial'
        ],
        [
            'type' => 'name',
            'value' => 'Mercury'
        ],
        [
            'type' => 'diameter',
            'value' => 4879
        ],
        [
            'type' => 'distance from sun',
            'value' => 58
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'terrestial'
        ],
        [
            'type' => 'name',
            'value' => 'Venus'
        ],
        [
            'type' => 'diameter',
            'value' => 12104
        ],
        [
            'type' => 'distance from sun',
            'value' => 108
        ],
        [
            'type' => 'moons',
            'value' => 0
        ],
        [
            'type' => 'avg surface temperature',
            'value' => 462
        ]
    ],
    [
        [
            'type' => 'composition',
            'value' => 'terrestial'
        ],
        [
            'type' => 'name',
            'value' => 'Pluto'
        ],
        [
            'type' => 'diameter',
            'value' => 2376
        ],
        [
            'type' => 'moons',
            'value' => 5
        ],
        [
            'type' => 'region',
            'value' => 'Kuiper belt'
        ]
    ],
];

$solarSystemEntities = flattenArrays($solarSystemEntities);

$solarSystemEntities = alignKeys($solarSystemEntities);

$entitiesByGroup = extractGroup($solarSystemEntities, ['Jovian', 'terrestial', 'Cheese']);
$entitiesByKeyGroup = extractGroup($solarSystemEntities, ['',0], 'moons');

print_r($entitiesByGroup);
print_r($entitiesByKeyGroup);

?>