<?php

$varX = 2;
$count = 0;

function orpro($num) {
    return 2 ** $num;
}

while(orpro($varX) < 1000000) {
    $varX = $varX * 2;
    $count++;
}

print "Number of loop cycles: \t" . $count . "\n";
print "Power raised to: \t" . $varX . "\n";

?>