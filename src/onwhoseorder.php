<?php

function restockInventory($currentInventory, $newStock) {
    $stock = $currentInventory;
    foreach ($newStock as $phoneName => $stockCount) {
        if ($stockCount)
        $stock[$phoneName] += $stockCount;
    }
    return $stock;
}

// please take note of the use of the & symbol in the function's parameters!
// observe how it is different from $currentInventory in the restockInventory() function
function buyFromInventory(&$inventory, $product, $amount = 1) {
    if ($amount > $inventory[$product]) {
        $amountLeft = $amount - $inventory[$product];
        $inventory[$product] = 0;
        return $amountLeft;
    }
    else {
        $inventory[$product] -= $amount;
        return 0;
    }
}

$newStock = [
    "iPhoneXS" => 100,
    "iPhoneXR" => 100
];


// Action 1
$inventory = json_decode(file_get_contents("./phonestock.json"), true);

// Action 2
$inventory = restockInventory($inventory, $newStock);

// Action 3
$itemToBuy = array_rand($inventory, 1);
buyFromInventory($inventory, $itemToBuy, 1);

// Action 4
$purchaseResult = buyFromInventory($inventory, "iPhone6", 10);
$amountBought = 10 - $purchaseResult;
echo "Bought " . $amountBought . " phones. \n";

// Action 5
$newInventory = [];
foreach ($inventory as $item => $stock) {
    if ($stock >= 100) {
        $newInventory[$item] = $stock;
    }
    else {
        echo "{$item} discontinued.\n";
    }
}
$inventory = $newInventory;

?>