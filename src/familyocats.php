<?php

$catFamily = [
    [
        "name"  => "Keith",
        "age"   => 2,
        "type"  => "British Shorthair",
        "sex"   => "M",
        "tabby" => false
    ],
    [
        "name"  => "Molly",
        "age"   => 5,
        "type"  => "Maine Coon",
        "sex"   => "F",
        "tabby" => true
    ],
    [
        "name"  => "Dawn",
        "age"   => 1,
        "type"  => "Egyptian Mau",
        "sex"   => "F",
        "tabby" => true
    ],
    [
        "name"  => "Poppy",
        "age"   => 1,
        "type"  => "Scottish Fold",
        "sex"   => "F",
        "tabby" => false
    ],
    [
        "name"  => "Scotty",
        "age"   => 1,
        "type"  => "Scottish Fold",
        "sex"   => "M",
        "tabby" => true
    ],
    [
        "name"  => "Dempsy",
        "age"   => 8,
        "type"  => "Siamese",
        "sex"   => "M",
        "tabby" => false
    ],
    [
        "name"  => "Finn",
        "age"   => 5,
        "type"  => "American Shorthair",
        "sex"   => "M",
        "tabby" => true
    ],
    [
        "name"  => "Vella",
        "age"   => 3,
        "type"  => "British Shorthair",
        "sex"   => "F",
        "tabby" => false
    ]
];

// Add a new member to the family
$catFamily[] = [
    "name"  => "Brenda",
    "age"   => 2,
    "type"  => "British Shorthair",
    "sex"   => "F",
    "tabby" => false
];

// Who are the feminine felines?
foreach ($catFamily as $cat) {
    if ($cat['sex'] == "F") {
        echo $cat['name'] . "\n";
    }
}

// Our collection of traits
$characteristics = [
    "curious",
    "foody",
    "perpetual napper",
    "affectionate",
    "playful"
];

// Let RNGeebus decide the cat's traits
$catTraits = [];
foreach ($catFamily as $cat) {
    $catTraits[$cat['name']] = $characteristics[rand(0,4)];
}

// Let's see the results of the trait lottery
print_r($catTraits);

// The time has come for the great cat schism
$newTabbyFam = [];
foreach ($catFamily as $index => $cat) {
    if ($cat['tabby']) {
        $newTabbyFam[] = $cat;
        unset($catFamily[$index]);
    }
}
// Optional re-indexing of our array
$catFamily = array_values($catFamily); 

print_r($catFamily); // our original cat family
print_r($newTabbyFam); // the new tabby cat family

// An alternative way to do the schism is to create a new array for the non-tabbies.

?>