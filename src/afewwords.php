<?php

function tellMeAbout($myObjects, $adjectives, $verbs, $limit = -1) {
    /** 
     * Valid values for $limit: 0 <= $limit <= count($myObjects)
     * So we can use a default of -1
     * 
     * If default or $limit is too big, $limit is however many elements we have in $myObjects
     */
    if ($limit > count($myObjects) || $limit < 0) {
        $limit = count($myObjects);
    }

    $result = [];

    // Using regular for loop
    // for ($i = 0; $i < $limit; $i++) {
    //     $adj = $adjectives[array_rand($adjectives, 1)];
    //     $verb = $verbs[array_rand($verbs, 1)];
    //     $result[] = $adj . " " . $verb . " " . $myObjects[$i];
    // }
    
    // Using foreach loop
    foreach ($myObjects as $index => $currObject) {
        // Remember index counts from 0, whereas the count($array) starts from 1!
        if ($index > $limit-1) {
            break;
        }
        $adj = $adjectives[array_rand($adjectives, 1)];
        $verb = $verbs[array_rand($verbs, 1)];
        $result[] = $adj . " " . $verb . " " . $currObject;
    }

    return $result;
}

print_r(tellMeAbout(['dog', 'cat', 'bear'],['excited', 'sleepy'],['sitting', 'crying'], 2));


?>