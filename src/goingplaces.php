<?php

/**
 * Describe network with dictionary
 * Write function to build list of all possible routes between A and B
 * Write a function to calculate the cost of a given route
 * Write a function that will calculate time take for a route
 * Write a function that will figure out all routes given a condition
 *  * fastest (time)
 *  * cheapest (cost)
 *  * least number of hops (this will be an exercise)
 *  
 * If a condition was given, only return the route matching that condition
 * If no condition given, return all conditions, and state which they are in dictionary
 *  * cheapest => [], fastest => []
 * 
 */

// graph representated as nodes with corresponding edges
$cityDestinations = [
    'Avareen' => ['Essiya', 'Freece'],
    'Emerind' => ['Essiya', 'Freece'],
    'Essiya'  => ['Avareen', 'Orsa', 'Freece', 'Emerind'],
    'Freece'  => ['Avareen', 'Essiya', 'Emerind'],
    'Orsa'    => ['Essiya']  
];

// graph respresented as edges only
$transportLinks = [
    ['Avareen', 'Essiya'],
    ['Avareen', 'Freece'],
    ['Essiya', 'Orsa'],
    ['Essiya', 'Freece'],
    ['Essiya', 'Emerind'],
    ['Emerind', 'Freece'],
];

// Example route representation for traveling from Avareen to Emerind
$routeExample = ['Avareen', 'Freece', 'Emerind'];

// 
/**
 * Function has to figure out all paths from A to B - this is a recursive algorithm!
 * Base case, it's found itself as a node, we won't do any isolated nodes since it's pointless in this context
 * 
 * Given origin, find all valid hops and return a list of this edges
 * E.g. From Avareen, it should return [['Avareen', 'Essiya'],['Avareen', 'Freece']]
 * 
 * First, is the last element in any of these our destination? If so, add that to our list of valid routes.
 * Remove that list from our proto-routes
 * 
 * Now at this state, each proto-route has a corresponding hope list that excludes itself.
 * Such that ['Avareen', 'Essiya']'s next hop cannot be 'Avareen' or 'Essiya'
 * Given this
 * 
 * 
 * Internal:
 * $cities
 * $routesFound - the result
 * $protoroutes - WIP
 * 
 * External:
 * $cityDestinations
 */

function getAllRoutesBetween($origin, $destination, $networkGraph) {
    $routesFound = [];
    $protoRoutes = [];

    $hopDestinations = $networkGraph[$origin];
    foreach ($hopDestinations as $hopOption) {
        // creates first hope
        $protoRoutes[] = [$origin, $hopOption];
        if ($hopOption == $destination) {
            $routesFound[] = [$origin, $hopOption];
        }
    }
    return $routesFound;

    // subsequent loops, we need our proto-routes and our cities
    // Our next hopes will be array_diff($all hop options, $currentProtoRoute) <-- give us cities not in route so far
    // for each hop left, create new proto route
    // if last element in protoRoute is $destination, add to routesFound, else do nothing
    // if array_diff is empty, hit base and backout recursion
}

function buildRoutes($protoRoutes, $networkGraph) {
    // protoRotues is [[],[]]
    // currProtoRoute is ['','']
    
    $updateProtoRoutes = []; 

    if (!empty($protoRoutes)) {
        $currCity = array_values(array_slice($protoRoutes[0], -1))[0];
        $hopOptions = array_diff($networkGraph[$currCity], $protoRoutes[0]);

        if (!empty($hopOptions)) {
            if (count($hopOptions) > 1) {

            }
            else {
                
            }
        }
        else {
            $updateProtoRoutes[] = $protoRoutes[0];
            $protoRoutes = array_diff($protoRoutes, $updateProtoRoutes);
        }
    }
    
}
