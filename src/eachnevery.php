<?php

// A sea of names
$seas = [
    "Black",
    "Mediterranean",
    "Dead",
    "Baltic",
    "North",
    "Irish",
    "Arabian",
    "Bering",
    "Java"
];

foreach($seas as $sea) {
    echo $sea . "\n";
}

$shortSeas = [];

foreach($seas as $sea) {
    if (strlen($sea) < 6) {
        $shortSea[] = $sea;
    }
}

print_r($shortSea);

?>