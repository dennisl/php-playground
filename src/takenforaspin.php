<?php

/**
 * Testing the following skills
 * 
 * 1. Syntax of data structures
 * 2. Tracking function calls and their resolution
 * 3. Complex filtering with conditionals
 */

/**
 * Enemy types
 * 1. Shield bearer - on hit, player is stunned for one turn - cannot be stunned consecutively - charges fusion rifle (reduce reload by 1 turn)
 * 2. Captain - tanky character
 * 3. Boop drone - weak but numerous
 * 4. Bean bearer - healer - damage will actually heal a random enemy (can heal self too)
 * 5. Knight - high damage, on hit, player gets a free retaliatory hit on it with 100% accuracy
 */

$theTaken = [
    "Phalanx Shield Basher" => [
        "maxhp" => 125,
        "accuracy" => 25,
        "damagePerHit" => 6
    ],
    "Captain" => [
        "maxhp" => 200,
        "accuracy" => 17,
        "damagePerHit" => 8
    ],
    "Boop Drone" => [
        "maxhp" => 50,
        "accuracy" => 10,
        "damagePerHit" => 2
    ],
    "Bean Bearer" => [
        "maxhp" => 70,
        "accuracy" => 40,
        "damagePerHit" => 5
    ],
    "Knight" => [
        "maxhp" => 100,
        "accuracy" => 40,
        "damagePerHit" => 20
    ]
];

// enemy makeup for the room
$room1 = [
    "Phalanx Shield Basher" => 1,
    "Captain" => 6,
    "Boop Drone" => 14,
    "Bean Bearer" => 2,
    "Knight" => 8
]; 

/**
 * Player weapons
 * 1. Regular rifle - rolls for crit chance 
 * 2. Fusion rifle - high damage multibolt weapon - can crit - 2 turn cooldown
 */
function simFusionRifle($boltCount = 5, $accuracy = 0.6) {
    $cumulativeDamage = 0;
    for ($i = 0; $i < $boltCount; $i++) {
        $cumulativeDamage += rand(14, 20);
    }
    return round($cumulativeDamage * $accuracy);
}

function simCritChance($damage = 20, $multiplier = 2, $critChance = 25) {
    if (rand(0, 100) < $critChance) {
        return $damage * $multiplier;
    }
    else {
        return $damage;
    }
}

function genCombatants($enemyInfo, $spawnTable) {
    $combatants = [];
    foreach ($spawnTable as $type => $number) {
        for ($i=0; $i < $number; $i++) {
            $combatants[$type." ".$i] = [
                "type" => $type,
                "hp" => rand($enemyInfo[$type]["maxhp"]/2, $enemyInfo[$type]["maxhp"])
            ];
        }
    }
    return $combatants;
}

// generate enemy health table
$combatants = genCombatants($theTaken, $room1);


/**
 * Begin combat sim
 */
$playerHp = 1200;
$playerStuns = 0;
$fusionCharge = 0;
$stunCooldown = 2; // can be stunned every other turn at most, 
$turns = 0;
while (count($combatants) > 0) {
    // Simulate enemy combatant turns first (they all attack you first, bit unfair but meh, too bad)
    foreach ($combatants as $enemyId => $enemyInfo) {
        if (rand(0,100) < $theTaken[$enemyInfo["type"]]["accuracy"]) {
            if ($enemyInfo["type"] != "Bean Bearer") {
                $playerHp -= $theTaken[$enemyInfo["type"]]["damagePerHit"];
                // echo "The player has taken damage from {$enemyId}\n";
            }
            else {
                $healTarget = array_rand($combatants, 1); 
                $combatants[$healTarget]["hp"] += $theTaken[$enemyInfo["type"]]["damagePerHit"];
                // echo "{$healTarget} was healed for 5hp!\n";
            }
            if ($enemyInfo["type"] == "Phalanx Shield Basher" && $playerStuns == 0) {
                $playerStuns += $stunCooldown;
                $fusionCharge += 1;
                // echo "The player has been stunned by {$enemyId} and will miss a turn!\n";
            }
            if ($enemyInfo["type"] == "Knight") {
                if ($fusionCharge > 1){
                    $combatants[$enemyId]["hp"] -= simCritChance(simFusionRifle());
                    // echo "Free retaliation hit with fusion on {$enemyId}!\n";
                    $fusionCharge = 0;
                }
                else {
                    $combatants[$enemyId]["hp"] -= simCritChance();
                    $fusionCharge += 1;
                    // echo "Free retaliation on {$enemyId}!\n";
                }
                if ($enemyInfo["hp"] < 1){
                    // echo "You managed to kill {$enemyId}!\n";
                    unset($combatants[$enemyId]);
                } 
            }
        }
        if ($playerHp < 1) {
            break 2;
        }
    }

    // simulate player attack - yes, you only get a single attack per turn but it's 100% accurate!
    if ($playerStuns < $stunCooldown) {
        $target = array_rand($combatants, 1);
        if ($fusionCharge > 1){
            $combatants[$target]["hp"] -= simCritChance(simFusionRifle());
            $fusionCharge = 0;
            // echo "You hit {$target} with your fusion rifle!\n";     
        }
        else {
            $combatants[$target]["hp"] -= simCritChance();
            $fusionCharge += 1;
            // echo "You damage {$target}.\n";
        }
        if ($combatants[$target]["hp"] < 1){
            // echo "You managed to kill {$target}!\n";
            unset($combatants[$target]);
        }
    }
    if ($playerStuns > 0) {
        $playerStuns -= 1;
    }
    // sleep (1); // if you want to enter slow mode, uncomment this line
    $turns++;
}

/**
 * Print results
 */
if ($playerHp < 1) {
    echo "Player died. There were still " . count($combatants) . " enemies remaining.\n";
    print_r($combatants);
}
else {
    echo "It took you {$turns} turns to clear the room with {$playerHp} hp left!\n";
}


?>