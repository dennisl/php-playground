# php-playground

## Learning Topics

* Syntax (statements, expressions, code blocks)
* Variables (creation and usage)
* Data structures (lists and dictionaries)
* Conditionals (if, elseif, else)
* Loops (while, for, foreach)
* Functions (creation and usage)
* Solving basic problems

### Syntax

This is an implicit topic which you'll pick up over the course of the other exercises.

### Variables

Another topic that will be implicitly exercised in later sections. But once again, the few exercises will be a reference for later exercises.

### Data structures

Special case of variables, but with a bit more fun stuff. Get accustomed to the syntax for creating lists and dictionaries, learn how to modify them by adding and removing elements and how to access elements in them.

A few exercises also to test how you cope with creating your own data structures with regular english prompts. After all, you need to be able to select when to deploy any given tool at your disposal when solving a problem. Do you know when to use a list, when to use a dictionary. What about when it is appropriate to nest, what about combining lists and dicts.

### Conditionals

Conceptually a key one becauses it decision making for computers.

Learn the core syntax for `if` statements, your first encounter of code-blocks and managing the flow of your code. Also playing around with contexts where `elseif` usage is appropriate and using the catch all `else`.

There will be a few exercises that use a few combinations of ifs, elseifs and else. Then a few exercises which are in plain english that will test your ability to use the 3 keywords for yourself.

### Loops

Another core skill to pick up since computers are all about doing stuff over and over again, quickly.

Learn the core concepts of looping - doing something repeatedly until a condition is met to stop. Pick up the different syntax for the 3 main types of loops. You'll practice this a few times to get used to writing loops.

Can you decide for yourself when to use the right loop? This is key to testing whether you understand the loops, their use cases and thus when best to deploy them.

### Functions

The beginnings of structuring code. This is a major building block for structuring code and governs a lot of good practices in programming.

Understand the concept that functions take an input and gives an output. This is something you've learnt in GCSE maths, but is now beyond the realm of just numbers!

Learn the syntax to create functions and then use/call them. Know the terminology used to describes the inputs for functions.

The exercises will build from simple syntax practice to examples with one or more inputs. Then using functions in context including nested function calls and using functions in control flow (conditionals and loops).

### Solving basic problems

A few combinatorial exercises, first with some guidance on the tools expected to be deployed and later exercises written in plain english without hinting.

These will test your ability to decide when to use the tools you've picked up from previous exercises. Do you know it's time to use a conditional, what about a loop? Are you suitably packaging up code into functions?

Once you hit the more open ended exercises, it will examine your general problem solving skills. Are you able to break down a larger problem into little bytes, or are you just diving in head first before taking a step back to plan your approach.

This will also be an opportunity to introduce to you (if you havent already used them) some tools to help you break down the problem:

* Flowcharts
  * More classical approach involving the usage of tasks/actions and decisions
  * Perhaps you'll have seen these before in the business world
* State diagrams
  * Introducing the concept of state
  * Start and end states
  * State changes

State diagrams are a powerful concept that focuses on data/information and how you are changing it. It aligns more closely to mathematics whereby when solving equations, each line may represent a state and the changes between each line constitute a state change.